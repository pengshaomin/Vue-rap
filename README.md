###  概述


Vue-rap 可以在不使用大量前端工具(如npm,webpack,Browserify等)的情况下快速构建基于 Vue的秒速打开边用边下载的流应用(单页面应用);

### 优点



* 依赖小(只需要引用 vue 就可以了),学习成本低,上手快
* vue-rap拥有类似 .vue 的单页面组件;
* vue-rap拥有可以方便使用的路由系统,路由秉承约定大约配置,可以快速路由,无限拓展;
* vue-rap拥有强大的缓存机制,应用支持秒级打开,边使用边下载,可以使用 vue-rap 构建流应用;

[查看文档](https://www.kancloud.cn/tengzhinei/vue-rap/550001)



### DEMO
下载源码后部署项目 然后直接访问   http://域名/example/admin.html

或直接访问线上 demo http://tengzhinei.gitee.io/vue-rap/example/admin.html

### 基于 vue-rap的后台框架 
http://lyadminui.magcloud.net/ 
开源地址 https://gitee.com/tengzhinei/ly-admin-ui

<div align="center">
  <img src="https://gitee.com/tengzhinei/ly-admin-ui/raw/master/images/1.jpeg">
  <img  src="https://gitee.com/tengzhinei/ly-admin-ui/raw/master/images/2.jpeg">
  <img  src="https://gitee.com/tengzhinei/ly-admin-ui/raw/master/images/3.jpeg">
  <img  src="https://gitee.com/tengzhinei/ly-admin-ui/raw/master/images/4.jpeg">
</div>


